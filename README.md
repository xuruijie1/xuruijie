git status			// 查看改变状态

git diff			// 查看变更内容

git add xxx			// 跟踪指定的文件

git add . 			// 跟踪所有改动过的文件（到暂存区）